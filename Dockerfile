FROM ruby:2.6.5
MAINTAINER XYenon <i@xyenon.bid>
RUN apt-get update && apt-get install -y build-essential libmariadb-dev
RUN mkdir /app
WORKDIR /app
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
ENV RAILS_ENV production
RUN gem install bundle && bundle install
COPY . /app

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

CMD bundle exec rake db:migrate && bundle exec rails server -b 0.0.0.0 -p 3000 -e production