class ApplicationController < ActionController::API

  protected

  def check_login
    render json: { msg: '未登录' }, status: :forbidden if current_user.blank?
  end

  def current_user
    if request.headers['Authorization']&.start_with?('Bearer')
      jwt_token = request.headers['Authorization'].split&.last
      jwt_info = User.decoded_jwt_infos(jwt_token)
      @current_user ||= User.find_by_id(jwt_info['id'])
    end
  rescue JWT::ExpiredSignature, JWT::VerificationError, JWT::DecodeError
    nil
  end
end
