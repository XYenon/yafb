class Api::ArticlesController < ApplicationController
  before_action :check_login, only: [:create, :destroy]

  def index
    Article.find_each do |article|
      article.viewers += 1
      article.save
    end
    articles = Article.all.reverse_order
    render json: { articles: articles }
  end

  def create
    unless @current_user.admin?
      return render json: { msg: '没有权限' }, status: :forbidden
    end

    Article.create(title: params[:title],
                   content: params[:content],
                   date: Time.now)
  end

  def destroy
    unless @current_user.admin?
      return render json: { msg: '没有权限' }, status: :forbidden
    end

    article = Article.find_by(id: params[:id])
    return render json: { msg: '找不到该文章' }, status: :not_found unless article

    article.destroy
    render body: nil
  end
end
