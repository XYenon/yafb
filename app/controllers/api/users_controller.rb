class Api::UsersController < ApplicationController
  before_action :check_login

  def show
    result = { stu_id: @current_user.stu_id,
               name: @current_user.name,
               admin: @current_user.admin }
    if params[:course_id]
      course = Course.find_by_course_id(params[:course_id])
      return render json: { msg: '找不到该课程' }, status: :not_found unless course

      result[:score] = course.scores.find_by(user: @current_user)&.score ||
                       '未学此课'
    end
    render json: result
  end
end
