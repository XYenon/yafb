class Api::TokensController < ApplicationController
  def create
    stu_id = params[:stu_id]
    stu_password = params[:stu_password]
    user = User.find_by_stu_id(stu_id)
    if user
      auth_user(user, stu_password)
    else
      create_user(stu_id, stu_password)
    end
  end

  private

  def auth_user(user, stu_password)
    result = user.auth(stu_password)
    if result[:status]
      render json: { token: user.generate_jwt_token }
    else
      render json: { msg: result[:msg] }, status: :unauthorized
    end
  end

  def create_user(stu_id, stu_password)
    result = User.auth_create(stu_id, stu_password)
    if result[:status]
      render json: { token: result[:user].generate_jwt_token }
    else
      render json: { msg: result[:msg] }, status: :unauthorized
    end
  end
end
