class Api::ScoresController < ApplicationController
  before_action :check_login

  def show
    scores = @current_user.scores.map do |score|
      { course: score.course, score: score }
    end
    render json: scores
  end

  def create
    result = Score.get_scoretables(@current_user.stu_id,
                                   @current_user.stu_password)
    if result[:status]
      scores = result[:scores]
      return render json: { msg: '获取成绩失败' } if scores.blank?

      Score.reload_scores(@current_user, scores)
      render json: scores
    else
      render json: { msg: result[:msg] }, status: :forbidden
    end
  end
end
