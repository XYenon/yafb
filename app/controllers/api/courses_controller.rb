class Api::CoursesController < ApplicationController
  def index
    start = params[:start] || 0
    count = params[:count] || 20
    courses = Course.limit(count).offset(start).map do |course|
      { name: course.name, course_id: course.course_id,
        count: course.analysis_score[:count] }
    end
    render json: { count: Course.count,
                   courses: courses }
  end

  def show
    course = Course.find_by_course_id(params[:id])
    return render json: { msg: '没有该课程' }, status: :not_found unless course

    render json: { course: course, scores: course.scores_count,
                   analysis: course.analysis_score }
  end

  def search
    courses = Course.where(Course.arel_table[:name]
                               .matches("%#{params[:keyword]}%"))
                    .map do |course|
      { name: course.name, course_id: course.course_id,
        count: course.analysis_score[:count] }
    end
    render json: { count: courses.size, courses: courses }
  end
end
