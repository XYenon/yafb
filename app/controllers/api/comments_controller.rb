class Api::CommentsController < ApplicationController
  before_action :check_login, only: [:create, :destroy]
  before_action :current_user, only: [:index]

  def index
    start = params[:start] || 0
    count = params[:count] || 10
    all_count = nil
    comments = if params[:course_id]
                 course = Course.find_by_course_id(params[:course_id])
                 return render json: { msg: '没有该课程' }, status: :not_found unless course

                 all_count = course.comments.count
                 course.comments.reverse_order.limit(count).offset(start)
               else
                 Comment.all.reverse_order.limit(count).offset(start)
               end
    comments = comments.map do |comment|
      user = comment.user
      course = comment.course
      score = comment.course.scores.select('score')
                     .find_by(user: user)&.score || '未学此课'
      { comment: { id: comment.id, content: comment.content,
                   date: comment.date, my: user == @current_user },
        course: { id: course.course_id, name: course.name },
        user: { name: comment.anonymous ? '匿名' : user.name, score: score } }
    end
    render json: { comments: comments, count: all_count }
  end

  def create
    course = Course.find_by_course_id(params[:course_id])
    return render json: { msg: '找不到该课程' }, status: :not_found unless course

    comment = course.comments.create(user: @current_user,
                                     course: course,
                                     anonymous: params[:anonymous],
                                     content: params[:content],
                                     date: Time.now)
    render json: comment, status: :created
  end

  def destroy
    comment = Comment.find_by(id: params[:id])
    return render json: { msg: '找不到该评论' }, status: :not_found unless comment

    unless @current_user.admin? || comment.user == @current_user
      return render json: { msg: '没有权限' }, status: :forbidden
    end

    render body: nil if comment.destroy
  end
end
