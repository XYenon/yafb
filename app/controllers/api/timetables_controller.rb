class Api::TimetablesController < ApplicationController
  before_action :check_login

  def show
    render json: @current_user.timetables
  end

  def create
    result = Timetable.get_timetable(@current_user.stu_id,
                                     @current_user.stu_password)
    if result[:status]
      timetable = result[:timetable]
      return render json: { msg: '获取课程表失败' } if timetable.blank?

      Timetable.reload_timetable(@current_user, timetable)
      render json: timetable
    else
      render json: { msg: result[:msg] }, status: :forbidden
    end
  end
end
