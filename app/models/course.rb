class Course < ApplicationRecord
  self.primary_key = 'course_id'

  has_many :scores
  has_many :comments

  def scores_count
    scores_count_list = {}
    if exam_method == '考试'
      (0..100).each { |i| scores_count_list[i.to_s.to_sym] = 0 }
    else
      scores_count_list = { 不及格: 0, 及格: 0, 中等: 0, 良好: 0, 优秀: 0 }
    end
    scores.find_each do |score|
      next if score.score.include?('-')

      scores_count_list[score.score.to_sym] += 1
    end
    scores_count_list
  end

  def analysis_score
    excellent_count = 0
    fail_count = 0
    if exam_method == '考试'
      scores.find_each do |score|
        next if score.score.include?('-')

        excellent_count += 1 if Integer(score.score) >= 90
        fail_count += 1 if Integer(score.score) < 60
      end
    else
      excellent_count = scores.where('score = ?', '优秀').size
      fail_count = scores.where('score = ?', '不及格').size
    end
    { count: scores.count,
      excellent_count: excellent_count, fail_count: fail_count }
  end
end
