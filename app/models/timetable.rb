class Timetable < ApplicationRecord
  belongs_to :user

  TIMETABLE_URL = 'https://edusys.wvpn.hrbeu.edu.cn/jsxsd/xskb/xskb_list.do'.freeze

  def self.get_timetable(stu_id, stu_password)
    result = User.edusys_login(stu_id, stu_password)
    return result unless result[:status]

    cookies = result[:cookies]
    time = Time.now
    params = {'cj0701id': '',
              'zc': '',
              'demo': '',
              'sfFD': 1}
    params['xnxq01id'] = if time.month == 1 || (time.month == 2 && time.day <= 20)
                           "#{time.year - 1}-#{time.year}-1"
                         elsif time.month < 8 || (time.month == 8 && time.day <= 23)
                           "#{time.year - 1}-#{time.year}-2"
                         else
                           "#{time.year}-#{time.year + 1}-1"
                         end
    response = HTTP.cookies(cookies).post(TIMETABLE_URL, form: params)
    doc = Nokogiri::HTML(response.body.to_s)
    timetable_table_trs = doc.xpath('//table[@id="kbtable"]/tr')[1..-1]
    courses = []
    timetable_table_trs.each_with_index do |tr, session_index|
      kbcontents = Nokogiri::HTML(tr.to_s).xpath('//div[@class="kbcontent"]')
      kbcontents.each_with_index do |kbcontent, day_index|
        courses_info = kbcontent.to_html.split('---------------------')
        courses_info.each do |course_info|
          course_info = Nokogiri::HTML(course_info.to_s)
          course = {}
          teacher = course_info.xpath('//font[@title="老师"]').text
          next if teacher.empty?

          course[:teacher] = teacher
          course[:week] = course_info.xpath('//font[@title="周次"]').text
          course[:small_session] = course_info.xpath('//font[@title="节次"]').text
          course[:room] = course_info.xpath('//font[@title="教室"]').text
          course[:name] = course_info.text.split(teacher)[0]
          course[:day] = day_index + 1
          course[:session] = session_index + 1
          courses.push course
        end
      end
    end
    {status: true, timetable: courses}
  rescue => e
    logger.warn { 'Get Timetable Failed: ' + e.message }
    {status: false, msg: e}
  end

  def self.reload_timetable(user, timetable)
    user.timetables.destroy_all
    timetable.each do |t|
      user.timetables.create(name: t[:name], teacher: t[:teacher], week: t[:week],
                             small_session: t[:small_session],
                             room: t[:room], day: t[:day], session: t[:session])
    end
  end
end
