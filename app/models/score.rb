class Score < ApplicationRecord
  belongs_to :course
  belongs_to :user

  SCORETABLE_URL = 'https://edusys.wvpn.hrbeu.edu.cn/jsxsd/kscj/cjcx_list'.freeze

  def self.get_scoretables(stu_id, stu_password)
    result = User.edusys_login(stu_id, stu_password)
    return result unless result[:status]

    cookies = result[:cookies]
    start_year = Integer(stu_id[0..3].to_s)
    semesters = []
    (start_year..Time.now.year).each do |year|
      semesters.push(year.to_s + '-' + (year + 1).to_s + '-1')
      semesters.push(year.to_s + '-' + (year + 1).to_s + '-2')
    end
    scores = []
    semesters.each do |semester|
      s = Score.get_scoretable(cookies, semester)
      scores += s if s
    rescue NoMethodError
      next
    end
    { status: true, scores: scores }
  rescue => e
    logger.warn { 'Get Scoretables Failed: ' + e.message }
    { status: false, msg: e }
  end

  def self.get_scoretable(cookies, semester)
    response = HTTP.cookies(cookies)
                   .post(SCORETABLE_URL,
                         form: { 'kksj': semester, 'kcxz': '',
                                 'kcmc': '', 'xsfs': 'all' })
    body_s = response.body.to_s
    return [] if body_s.include?('未查询到数据')

    doc = Nokogiri::HTML(body_s)
    scoretable_table_trs = doc.xpath('//table[@id="dataList"]/tr')[1..-1]
    scores = []
    scoretable_table_trs.each do |tr|
      tds = Nokogiri::HTML(tr.to_s).xpath('//td')[1..-1]
      score = {}
      tds.each_with_index do |td, td_index|
        case td_index
        when 0 # 开课学期
          score[:semester] = td.text
        when 1 # 课程编号
          score[:course_id] = td.text
        when 2 # 课程名称
          score[:course_name] = td.text
        when 3 # 成绩
          score[:score] = td.text
        when 4 # 学分
          score[:course_credit] = td.text
        when 5 # 总学时
          score[:course_hour] = td.text
        when 6 # 考核方式
          score[:course_exam_method] = td.text
        when 7 # 考试性质
          score[:exam_type] = td.text
        when 8 # 课程属性
          score[:course_attr] = td.text
        when 9 # 课程性质
          score[:course_type] = td.text
        when 10 # 通识教育选修课程类别
          score[:course_category] = td.text
        when 11 # 成绩标记
          score[:score_mark] = td.text
        end
      end
      scores.push(score)
    end
    scores
  rescue => e
    logger.warn { 'Get ScoreTable Failed: ' + e.message }
    false
  end

  def self.reload_scores(user, scores)
    user.scores.destroy_all
    scores.each do |score|
      course = Course.find_or_create_by(course_id: score[:course_id]) do |c|
        c.name = score[:course_name]
        c.credit = score[:course_credit]
        c.hour = score[:course_hour]
        c.exam_method = score[:course_exam_method]
        c.course_attr = score[:course_attr]
        c.course_type = score[:course_type]
        c.course_category = score[:course_category]
      end
      Score.create do |s|
        s.course = course
        s.user = user
        s.semester = score[:semester]
        s.score = score[:score]
        s.exam_type = score[:exam_type]
        s.score_mark = score[:score_mark]
      end
    end
  end
end
