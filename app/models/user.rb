class User < ApplicationRecord
  has_many :scores
  has_many :comments
  has_many :timetables

  CAS_LOGIN_URL = 'https://ehome.wvpn.hrbeu.edu.cn/'.freeze
  EDUSYS_INDEX_URL = 'https://edusys.wvpn.hrbeu.edu.cn/jsxsd/framework/main.jsp'.freeze

  def self.decoded_jwt_infos(token)
    JWT.decode(token, Rails.application.credentials.jwt_token,
               true, algorithm: 'HS256')[0]
  end

  def self.auth_create(stu_id, stu_password)
    info = User.edusys_login(stu_id, stu_password)
    return { status: false, msg: info[:msg] } unless info[:status]

    user = User.create do |u|
      u.stu_id = stu_id
      u.stu_password = stu_password
      u.name = info[:name]
    end
    { status: true, user: user }
  end

  def self.edusys_login(id, password)
    response = HTTP.follow.get(CAS_LOGIN_URL)
    doc = Nokogiri::HTML(response.body.to_s)
    login_post_url = 'https://cas-443.wvpn.hrbeu.edu.cn' +
                     doc.xpath('//div[@id="login"]/form')[0]['action']
    lt_value = doc.xpath('//input[@name="lt"]')[0]['value']
    execution_value = doc.xpath('//input[@name="execution"]')[0]['value']
    event_id_value = doc.xpath('//input[@name="_eventId"]')[0]['value']
    login_button_value = doc.xpath('//input[@name="submit"]')[0]['value']
    jsessionid = /jsessionid=\w*?\?/.match(login_post_url).to_s[11...-1]
    response = HTTP.cookies('JSESSIONID': jsessionid,
                            'MESSAGE_TICKET': '%7B%22times%22%3A0%7D')
                   .post(login_post_url,
                         form: { 'username': id,
                                 'password': password,
                                 'captcha': '',
                                 'lt': lt_value,
                                 'execution': execution_value,
                                 '_eventId': event_id_value,
                                 'submit': login_button_value })
    doc = Nokogiri::HTML(response.body.to_s)
    msg = doc.xpath('//div[@id="msg"]').first&.text
    return { status: false, msg: msg } if msg && !msg.include?('登录成功')

    cookies = {}
    response.cookies.each do |item|
      cookies[item.name] = item.value
    end
    while response['Location']
      response = HTTP.cookies(cookies).get(response['Location'])
      response.cookies.each do |item|
        cookies[item.name] = item.value
      end
    end

    response = HTTP.cookies(cookies).get(EDUSYS_INDEX_URL)
    while response['Location']
      response = HTTP.cookies(cookies).get(response['Location'])
      response.cookies.each do |item|
        cookies[item.name] = item.value
      end
    end
    doc = Nokogiri::HTML(response.body.to_s)
    name = doc.xpath('//div[@id="Top1_divLoginName"]').text.split('(').first
    { status: true, name: name, cookies: cookies }
  rescue => e
    logger.warn { 'EduSys Login Failed: ' + e.message }
    { status: false, msg: e }
  end

  def auth(password)
    if ActiveSupport::SecurityUtils.secure_compare(password, stu_password)
      { status: true }
    else
      reset_password(password)
    end
  end

  def reset_password(new_password)
    info = User.edusys_login(stu_id, new_password)
    return { status: false, msg: info[:msg] } unless info[:status]

    self.stu_password = new_password
    save
    { status: true }
  end

  def generate_jwt_token
    hmac_secret = Rails.application.credentials.jwt_token
    payload = {
      id: id,
      stu_id: stu_id,
      name: name,
      admin: admin,
      exp: (Time.now + 4.year).to_i
    }
    JWT.encode payload, hmac_secret, 'HS256'
  end
end
