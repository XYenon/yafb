# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_18_064619) do

  create_table "articles", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.integer "viewers", default: 0
    t.date "date"
  end

  create_table "comments", force: :cascade do |t|
    t.integer "user_id"
    t.integer "course_id"
    t.boolean "anonymous"
    t.text "content"
    t.date "date"
    t.index ["course_id"], name: "index_comments_on_course_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "course_id"
    t.string "name"
    t.string "credit"
    t.integer "hour"
    t.string "exam_method"
    t.string "course_attr"
    t.string "course_type"
    t.string "course_category"
  end

  create_table "scores", force: :cascade do |t|
    t.integer "course_id"
    t.integer "user_id"
    t.string "semester"
    t.string "score"
    t.string "exam_type"
    t.string "score_mark"
    t.index ["course_id"], name: "index_scores_on_course_id"
    t.index ["user_id"], name: "index_scores_on_user_id"
  end

  create_table "timetables", force: :cascade do |t|
    t.integer "user_id"
    t.string "name"
    t.string "teacher"
    t.string "week"
    t.string "small_session"
    t.string "room"
    t.integer "day"
    t.integer "session"
    t.index ["user_id"], name: "index_timetables_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "stu_id"
    t.string "stu_password"
    t.string "name"
    t.boolean "admin", default: false
    t.index ["stu_id"], name: "index_users_on_stu_id"
  end

end
