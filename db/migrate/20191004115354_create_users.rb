class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :stu_id
      t.string :stu_password
      t.string :name
      t.boolean :admin, default: false
    end
  end
end
