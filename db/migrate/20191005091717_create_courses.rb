class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.string :course_id # 课程编号
      t.string :name # 课程名称
      t.string :credit # 学分
      t.integer :hour # 学时
      t.string :exam_method # 考核方式
      t.string :course_attr # 课程属性
      t.string :course_type # 课程性质
      t.string :course_category # 通识教育选修课程类别
    end
  end
end
