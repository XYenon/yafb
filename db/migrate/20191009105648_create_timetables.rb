class CreateTimetables < ActiveRecord::Migration[6.0]
  def change
    create_table :timetables do |t|
      t.belongs_to :user
      t.string :name
      t.string :teacher
      t.string :week
      t.string :small_session
      t.string :room
      t.integer :day
      t.integer :session
    end
  end
end
