class CreateArticles < ActiveRecord::Migration[6.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :content
      t.integer :viewers, default: 0
      t.date :date
    end
  end
end
