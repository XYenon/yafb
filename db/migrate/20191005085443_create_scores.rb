class CreateScores < ActiveRecord::Migration[6.0]
  def change
    create_table :scores do |t|
      t.belongs_to :course
      t.belongs_to :user
      t.string :semester # 开课学期
      t.string :score # 成绩
      t.string :exam_type # 考试性质
      t.string :score_mark # 成绩标记
    end
  end
end
