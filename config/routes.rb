Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :api do
    post '/login', to: 'tokens#create'

    resource :user, only: [:show]
    resource :score, only: [:show, :create]
    resource :timetable, only: [:show, :create]

    resources :articles, only: [:index, :create, :destroy]
    resources :comments, only: [:index, :destroy]
    resources :courses, only: [:index, :show] do
      get 'search', on: :collection
      resource :user, only: [:show]
      resources :comments, only: [:index, :create]
    end
  end
end
